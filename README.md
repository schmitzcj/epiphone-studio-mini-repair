# Epiphone Studio Mini Amp Repair

![Amplifier Isometric Drawing](/Files/amp.png)

## Table of contents

* [Introduction](#introduction)
* [Symptoms](#symptoms)
* [Teardown](#teardown)
* [Amplifier Circuit](#amplifier-circuit)
* [Repair](#repair)
* [Final Results/Lessons Learned](#final-results)

## Introduction

When I was young, I expressed interest in playing the guitar to my parents. My father decided he would get me an Epiphone Les Paul PeeWee guitar. It was a neat guitar and came with a small, 1 watt, amplifier that could run off a 9v battery or 9v center-positive power supply. Center-positive power supplies. Those are a more common in the audio world than they are elsewhere, and that's where the trouble began.

At one point in time, the battery compartment for the amplifier lost its battery compartment cover. As a result, I wasn't able to keep a battery in the amplifier, so I decided that I would fish out a 9v power supply and give it a try. I remember clearly reading the symbols on the power supply and noticing they were different compared to the symbol on the amplifier. I knew that the symbols were different and knew bad things could happen, would happen, if I plugged it in. But I threw common sense to the wind and you can guess what happened there. And so, for well over a decade the guitar amplifier sat in storage, waiting for the day it would be repaired.

## Symptoms

The cause of the unit's failure was due to reversed polarity from the 9v power supply. The following symptoms were exhibited when powered correctly:

* Red power light would turn on and become dim
* Scratchy sound from the speakers when toggling the power switch
* Sometimes a dull hum from the speakers while turned on

## Teardown

The teardown is a relatively simple process. On the back of the amp, remove the four screws on the back panel and carefully remove it. Next, remove the two screws from the top of the amp that are holding the metal cover panel and circuit board in place. To separate the metal cover panel from the circuit board, remove the hex nuts from the audio jacks and the knobs and hex nuts from the volume and tone dials.

Be mindful of the wires going from the circuit board to both the battery compartment( located on the back panel) and the speaker mounted on the front. Desoldering these wires will make circuit board repairs easier.

## Amplifier Circuit

Now that we have the circuit board completely removed, we can look at the circuit board.

This is what the circuit board looks like:

![Circuit front](/Files/top.jpg)

![Circuit back](/Files/bottom.jpg)

Since there was no easily-found schematic online, I created one for it(a digital schematic will be created when I have time available):

![Circuit schematic](/Files/schematic.jpg)

As you can see, this is a fairly straighforward and simplistic design, including tone adjustment and an overdrive mode. 

## Repair

Repairing the circuit was a relatively straightforward process. Immediately upon tearing down the circuit there were noticeable scortch marks around and on the IC around the ground traces. Additionally, one of the capacitors connected to ground had expanded significantly. This makes sense, as these were some of the first components in the line of fire when I reverse the polarity by using the wrong power supply.

Removing the components was fairly easy, however some of the traces became lifted during the desoldering process. I am going to attribute this to the age and quality of the circuit board. This was easily remidied by running some wires across the board to the intended destinations.

![Bulging capacitor](/Files/capacitor.jpeg)

Here you can see the components in better light, after having been removed:

![Removed components](/Files/removed_components.png)

The amplifier IC used in the circuit, KIA6213s, is no longer in production. However, they were being sold on [eBay](https://www.ebay.com/itm/202692814644) (cheaper than purchasing from component sites) so I was still able to procure one. No need to find similar ICs or do additional modifications there.

The bulging capacitor used was also no longer in production, so plugging in the values in to a parts website like Digikey, I was able to obtain a suitable replacement.

As a last step in fixing the amplifier, I had to replace the battery compartment. Since there was no markings on the battery compartment to determine the brand, I ended up purchasing a new on from [DigiKey](https://www.digikey.com/en/products/detail/BX0023/708-1403-ND/1980761). This was a little more complicated, as the compartment dimensions were buried in the datasheets. Luckily there weren't many options. This was a tight fit, especially with the faux leather, but it worked.

# Final Results

Well, I thought I was done at first. Turns out that I didn't do a good enough job cleaning up the solder flux. The amplifier would operate fine, with some distortion, but quickly degrade into distorted, crackling sounds:

![Initial result](/Files/guitar_amp_initial.webm)

After having cleaned up the solder flux, I was left with a clean sounding result:

![Final result](/Files/guitar_amp_final.mp4)

The takeaways from this project are:
 - Ensure solder flux is thourougly cleaned up
 - Fixing things can be fun and maybe easier than you might think
